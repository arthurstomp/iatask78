package ufs.br.ia.task8;

import aima.core.probability.util.RandVar;
import aima.core.probability.domain.ArbitraryTokenDomain;

public class Task8RV {
	
	// Values for 
	public static final String AGE030 = "030";
	public static final String AGE3140 = "3140";
	public static final String AGE41100 = "41100";
	
	// Values for Nationality
	public static final String NATIONALITY_AMERICAN = "american";
	public static final String NATIONALITY_NON_AMERICAN = "non-american";
	
	// Values for Sport
	public static final String SPORT_LIKE = "like";
	public static final String SPORT_DONT = "doesntLike";
	
	// Values for Watch Tv
	public static final String WATCH_TV_A_LOT = "aLot";
	public static final String WATCH_TV_SOME = "some";
	public static final String WATCH_TV_NONE = "none";
	
	public static final RandVar AGE_RV = new RandVar("Age", 
			new ArbitraryTokenDomain(AGE030,AGE3140,AGE41100));

	public static final RandVar NATIONALITY_RV = new RandVar("Nationality",
			new ArbitraryTokenDomain(NATIONALITY_AMERICAN, NATIONALITY_NON_AMERICAN));
	
	public static final RandVar SPORT_RV = new RandVar("Sport",
			new ArbitraryTokenDomain(SPORT_LIKE, SPORT_DONT));
	
	public static final RandVar WATCH_TV_RV = new RandVar("WatchTv",
			new ArbitraryTokenDomain(WATCH_TV_A_LOT, WATCH_TV_SOME, WATCH_TV_NONE));
}
