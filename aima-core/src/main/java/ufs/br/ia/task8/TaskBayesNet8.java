package ufs.br.ia.task8;

import java.util.Map;

import org.junit.experimental.results.PrintableResult;

import aima.core.probability.CategoricalDistribution;
import aima.core.probability.bayes.*;
import aima.core.probability.bayes.approx.PriorSample;
import aima.core.probability.bayes.approx.RejectionSampling;
import aima.core.probability.bayes.exact.EliminationAsk;
import aima.core.probability.bayes.exact.EnumerationAsk;
import aima.core.probability.bayes.impl.BayesNet;
import aima.core.probability.bayes.impl.FullCPTNode;
import aima.core.probability.proposition.AssignmentProposition;
import aima.core.probability.util.RandVar;


public class TaskBayesNet8 {

	private static void printResults(String caseName, RandVar[] rv, AssignmentProposition[] ap,BayesNet bn) {
		
		System.out.println(caseName);
		
		// EnumerationAsk		
		System.out.println("Enumeration Ask");
		EnumerationAsk enumeration = new EnumerationAsk();
		CategoricalDistribution returnEnumerationAsk = enumeration.ask(rv, ap, bn);
		System.out.println(returnEnumerationAsk);
		
		// EliminationAsk
		System.out.println("Elimination Ask");
		EliminationAsk elimination = new EliminationAsk();
		CategoricalDistribution returnEliminationAsk = elimination.ask(rv, ap, bn);
		System.out.println(returnEliminationAsk);
		
		// Prior-Sample
		System.out.println("Prior-Sampling");
		PriorSample ps = new PriorSample();
		for (int i = 0; i < 10; i++) {
			Map resultPS = ps.priorSample(bn);
			System.out.println(resultPS);
		}
		
		// Rejection-Sampling
		System.out.println("Rejection-Sampling");
		RejectionSampling rs = new RejectionSampling();
		CategoricalDistribution returnRejectionSampling = rs.rejectionSampling(rv, ap, bn, 100000);
		System.out.println(returnRejectionSampling);
	}
	
	public static void main(String[] args){
		FiniteNode ageNode = new FullCPTNode(Task8RV.AGE_RV, new double[]{
				0.3, 0.6, 0.1
		});
		
		FiniteNode nationalityNode = new FullCPTNode(Task8RV.NATIONALITY_RV, new double[]{
			0.2, 0.8	
		});
		
		FiniteNode sportNode = new FullCPTNode(Task8RV.SPORT_RV, new double[]{
			// Age = 0 - 30 Nationality = American
			0.5, 0.5,
			// Age = 31 - 40 Nationality = American
			0.7, 0.3,
			// Age = 41 - 100 Nationality = American
			0.6, 0.4,
			// Age = 0 - 30 Nationality = Non-American
			0.8, 0.2,	
			// Age = 31 - 40 Nationality = Non-American
			0.4, 0.6,	
			// Age = 41 - 100 Nationality = Non-American
			0.1, 0.9		
		}, ageNode, nationalityNode);
		
		FiniteNode watchTvNode = new FullCPTNode(Task8RV.WATCH_TV_RV, new double[]{
			// Sport = Like
			0.7, 0.2, 0.1,
			// Sport = Doesn't
			0.5, 0.3, 0.2
		}, sportNode);
		
		BayesNet task8BN = new BayesNet(ageNode, nationalityNode, sportNode, watchTvNode);
		
		RandVar[] observationVars = new RandVar[]{Task8RV.SPORT_RV};
		
		// First Case
		AssignmentProposition ageAP = new AssignmentProposition(Task8RV.AGE_RV, Task8RV.AGE030);
		AssignmentProposition nationalityAP = new AssignmentProposition(Task8RV.NATIONALITY_RV, Task8RV.NATIONALITY_AMERICAN);
		AssignmentProposition[] evidenceProposition = new AssignmentProposition[]{ageAP,nationalityAP};
		
		TaskBayesNet8.printResults("Case #1", observationVars, evidenceProposition, task8BN);
		
		// Second Case
		ageAP = new AssignmentProposition(Task8RV.AGE_RV, Task8RV.AGE3140);
		nationalityAP = new AssignmentProposition(Task8RV.NATIONALITY_RV, Task8RV.NATIONALITY_AMERICAN);
		evidenceProposition = new AssignmentProposition[]{ageAP,nationalityAP};
		observationVars = new RandVar[]{Task8RV.WATCH_TV_RV};
		
		TaskBayesNet8.printResults("Case #2", observationVars, evidenceProposition, task8BN);
	}

}
