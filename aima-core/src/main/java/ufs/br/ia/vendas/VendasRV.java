package ufs.br.ia.vendas;

import aima.core.probability.util.RandVar;
import aima.core.probability.domain.ArbitraryTokenDomain;

/**
 * Rede bayeseana que representa um mapa causal, num contexto de decis�o sobre as
 * perspectivas de vendas de um determinado produto, elaborado por um especialista
 * O mapa expressa conhecimento qualitativo na forma de conceitos e rela��es causais entre estes conceitos.
 * O objetivo � caracterizar depend�ncias entre vari�veis em termos de explana��es e conseq��ncias
 * causais conforme expresso a seguir: (a) quanto mais alta a infla��o tanto mais altos ser�o os
 * juros; (b) quanto mais altos os juros tanto mais reduzidas ser�o as vendas; e (c) quanto mais
 * acertadas as a��es de marketing tanto maiores ser�o as vendas. 
 * 
 * Ao mapa causal o especialista acrescentou os estados dos conceitos (vari�veis), por exemplo, a
 * infla��o pode ser alta ou baixa. Em seguida ele estimou as probabilidades envolvidas que
 * refletem determinada situa��o em an�lise. No caso das vari�veis I e M s�o probabilidades
 * incondicionais e no caso das vari�veis J e V s�o probabilidades condicionadas �s vari�veis
 * �causadoras�. A figura corresponde agora a uma rede bayesiana pelo fato de estabelecer rela��es
 * probabil�sticas entre as vari�veis, possuir apenas conex�es direcionadas e n�o incluir ciclos. 
 * 
 * @author Arthur Rocha, Rafael Rabelo
 */

public class VendasRV {
	
	public static final RandVar Infacao = new RandVar("Infacao", 
			new ArbitraryTokenDomain("Alta", "Baixa"));

	public static final RandVar Juros = new RandVar("Juros",
			new ArbitraryTokenDomain("Altos", "Baixos"));
	
	public static final RandVar Marketing = new RandVar("Marketing",
			new ArbitraryTokenDomain("Certo", "Errado"));
	
	public static final RandVar Vendas = new RandVar("Vendas",
			new ArbitraryTokenDomain("Boas", "Ruins"));	

}
