package ufs.br.ia.vendas;

import java.util.Map;

import aima.core.probability.CategoricalDistribution;
import aima.core.probability.bayes.*;
import aima.core.probability.bayes.approx.PriorSample;
import aima.core.probability.bayes.approx.RejectionSampling;
import aima.core.probability.bayes.exact.EliminationAsk;
import aima.core.probability.bayes.exact.EnumerationAsk;
import aima.core.probability.bayes.impl.BayesNet;
import aima.core.probability.bayes.impl.FullCPTNode;
import aima.core.probability.proposition.AssignmentProposition;
import aima.core.probability.util.RandVar;
import ufs.br.ia.vendas.VendasRV;

/**
 * Rede bayeseana que representa um mapa causal, num contexto de decis�o sobre as
 * perspectivas de vendas de um determinado produto, elaborado por um especialista
 * O mapa expressa conhecimento qualitativo na forma de conceitos e rela��es causais entre estes conceitos.
 * O objetivo � caracterizar depend�ncias entre vari�veis em termos de explana��es e conseq��ncias
 * causais conforme expresso a seguir: (a) quanto mais alta a infla��o tanto mais altos ser�o os
 * juros; (b) quanto mais altos os juros tanto mais reduzidas ser�o as vendas; e (c) quanto mais
 * acertadas as a��es de marketing tanto maiores ser�o as vendas. 
 * 
 * Ao mapa causal o especialista acrescentou os estados dos conceitos (vari�veis), por exemplo, a
 * infla��o pode ser alta ou baixa. Em seguida ele estimou as probabilidades envolvidas que
 * refletem determinada situa��o em an�lise. No caso das vari�veis I e M s�o probabilidades
 * incondicionais e no caso das vari�veis J e V s�o probabilidades condicionadas �s vari�veis
 * �causadoras�. A figura corresponde agora a uma rede bayesiana pelo fato de estabelecer rela��es
 * probabil�sticas entre as vari�veis, possuir apenas conex�es direcionadas e n�o incluir ciclos. 
 * 
 * @author Arthur Rocha, Rafael Rabelo
 */

public class VendasNet {

	public static void main(String[] args){
		
		FiniteNode infacao = new FullCPTNode(VendasRV.Infacao, new double[]{
				//Infla��o Alta + Infla��o Baixa
				0.75, 0.25	
			});
		
		FiniteNode juros = new FullCPTNode(VendasRV.Juros, new double[]{
			//Juros Altos + Infla��o Alta | Juros Baixos + Infla��o Alta
			0.95, 0.05,
			//Juros Altos + Infla��o Alta | Juros Baixos + Infla��o Alta
			0.20, 0.80

		}, infacao);
		
		FiniteNode marketing = new FullCPTNode(VendasRV.Marketing, new double[]{
			//Marketing Certo + Marketing Errado
			0.9, 0.1	
		});
		
		FiniteNode vendas = new FullCPTNode(VendasRV.Vendas, new double[]{
			// Vendas Boas | Juros Altos | Marketing Certo 
				0.6,
			// Vendas Ruins | Juros Altos | Marketing Certo
				0.4,
			// Vendas Boas | Juros Altos | Marketing Errados
				0.2,
			// Vendas Ruins | Juros Altos | Marketing Errados
				0.8,
			// Vendas Boas | Juros Baixos | Marketing Certo
				0.9,
			// Vendas Ruins | Juros Baixos | Marketing Certo
				0.1,
			// Vendas Boas | Juros Baixos | Marketing Errados
				0.7,
			// Vendas Ruins | Juros Baixos | Marketing Errados
				0.30

		}, juros, marketing);
		
		BayesNet Vendas = new BayesNet(infacao, juros, marketing, vendas);
		
// 		//	Exemplo 1
//		AssignmentProposition infacaoAP = new AssignmentProposition(VendasRV.Infacao, "Alta");
//		AssignmentProposition jurosAP = new AssignmentProposition(VendasRV.Juros, "Altos");
//		AssignmentProposition marketingAP = new AssignmentProposition(VendasRV.Marketing, "Errado");

// 		//	Exemplo 2
//		AssignmentProposition infacaoAP = new AssignmentProposition(VendasRV.Infacao, "Baixa");
//		AssignmentProposition jurosAP = new AssignmentProposition(VendasRV.Juros, "Baixos");
//		AssignmentProposition marketingAP = new AssignmentProposition(VendasRV.Marketing, "Certo");
	
//		//	Exemplo 3		
//		AssignmentProposition infacaoAP = new AssignmentProposition(VendasRV.Infacao, "Alta");
//		AssignmentProposition jurosAP = new AssignmentProposition(VendasRV.Juros, "Baixos");
//		AssignmentProposition marketingAP = new AssignmentProposition(VendasRV.Marketing, "Errado");
		
		//	Exemplo 4		
		AssignmentProposition infacaoAP = new AssignmentProposition(VendasRV.Infacao, "Alta");
		AssignmentProposition jurosAP = new AssignmentProposition(VendasRV.Juros, "Altos");
		AssignmentProposition marketingAP = new AssignmentProposition(VendasRV.Marketing, "Certo");
		
		AssignmentProposition[] rsap = new AssignmentProposition[]{infacaoAP,jurosAP,marketingAP};
		RandVar[] rsrv = new RandVar[]{VendasRV.Vendas};
		
		// EnumerationAsk
		EnumerationAsk ea = new EnumerationAsk();
		CategoricalDistribution returnEnumerationAsk = ea.enumerationAsk(rsrv, rsap, Vendas);
		System.out.println("EnumerationAsk: " + returnEnumerationAsk);
		
		// EliminationAsk
		EliminationAsk ela = new EliminationAsk();
		CategoricalDistribution returnEliminationAsk = ela.eliminationAsk(rsrv, rsap, Vendas);
		System.out.println("EliminationAsk: " + returnEliminationAsk);
		
		// Prior-Sample
		PriorSample ps = new PriorSample();
		Map resultPS = ps.priorSample(Vendas);
		System.out.println("Prior-Sample: " + resultPS);
		
		// Rejection-Sampling
		RejectionSampling rs = new RejectionSampling();
		CategoricalDistribution cd = rs.rejectionSampling(rsrv, rsap, Vendas, 10);
		System.out.println("Rejection-Sampling: " + cd);
	}
}
