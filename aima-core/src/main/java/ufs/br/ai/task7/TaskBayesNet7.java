package ufs.br.ai.task7;

import java.util.Map;

import aima.core.probability.CategoricalDistribution;
import aima.core.probability.RandomVariable;
import aima.core.probability.bayes.*;
import aima.core.probability.bayes.approx.PriorSample;
import aima.core.probability.bayes.approx.RejectionSampling;
import aima.core.probability.bayes.exact.EliminationAsk;
import aima.core.probability.bayes.exact.EnumerationAsk;
import aima.core.probability.bayes.impl.BayesNet;
import aima.core.probability.bayes.impl.FullCPTNode;
import aima.core.probability.proposition.AssignmentProposition;
import aima.core.probability.util.RandVar;

public class TaskBayesNet7 {
	
	public static void main(String[] args){
		FiniteNode bolsaNode = new FullCPTNode(Task7RV.Bolsa , new double[]{
				0.11, 0.11, 0.11, 0.11, 0.11, 0.11, 0.11, 0.11, 0.12 //Foi necess�rio adicionar um a mais em uma das vari�veis para poder fechar a soma de 1 na TPC
		});
		
		// Cria nó da rede bayesiana com a probabilidades para cada caso.
		FiniteNode rendaFamiliarNode = new FullCPTNode(Task7RV.RendaFamiliar , new double[]{
				
			//DEZ
			0.01, 0.29, 0.70,
			
			//QUINZE
			0.03, 0.32, 0.65,
			
			//VINTE
			0.15, 0.45, 0.40,
			
			//VINTE E CINCO
			0.20, 0.50, 0.30,
			
			//TRINTA
			0.35, 0.40, 0.25,
			
			//TRINTA E CINCO
			0.52, 0.33, 0.15,
			
			//QUARENTA	
			0.65, 0.25, 0.10,
			
			//QUARENTA E CINCO
			0.75, 0.23, 0.02,
			
			//CINQUENTA
			0.80, 0.19, 0.01
			
		}, bolsaNode);
		
		// Cria nó da rede bayesiana com a probabilidades para cada caso.
		FiniteNode trabalhaNode = new FullCPTNode(Task7RV.Trabalha , new double[]{

				//DEZ
				0.50, 0.50,
				
				//QUINZE
				0.55, 0.45,
				
				//VINTE
				0.60, 0.40,
				
				//VINTE E CINCO
				0.65, 0.35,
				
				//TRINTA
				0.70, 0.30, 
				
				//TRINTA E CINCO
				0.75, 0.25, 
				
				//QUARENTA	
				0.75, 0.25, 
				
				//QUARENTA E CINCO
				0.75, 0.25, 
				
				//CINQUENTA
				0.75, 0.25
				
			}, bolsaNode);
		
		// Cria nó da rede bayesiana com a probabilidades para cada caso.
		FiniteNode conjugeNode = new FullCPTNode(Task7RV.Conjuge , new double[]{
				
				//DEZ
				0.50, 0.50,
				
				//QUINZE
				0.50, 0.50,
				
				//VINTE
				0.55, 0.45,
				
				//VINTE E CINCO
				0.60, 0.40,
				
				//TRINTA
				0.62, 0.38, 
				
				//TRINTA E CINCO
				0.64, 0.36, 
				
				//QUARENTA	
				0.66, 0.34, 
				
				//QUARENTA E CINCO
				0.68, 0.32, 
				
				//CINQUENTA
				0.70, 0.30
			}, bolsaNode);
		
		FiniteNode irmao = new FullCPTNode(Task7RV.Irmao , new double[]{
				
				//DEZ
				0.50, 0.50,
				
				//QUINZE
				0.55, 0.45,
				
				//VINTE
				0.60, 0.40,
				
				//VINTE E CINCO
				0.62, 0.38,
				
				//TRINTA
				0.64, 0.36, 
				
				//TRINTA E CINCO
				0.66, 0.34, 
				
				//QUARENTA	
				0.68, 0.32, 
				
				//QUARENTA E CINCO
				0.69, 0.31, 
				
				//CINQUENTA
				0.70, 0.30
			}, bolsaNode);
		
		// Cria nó da rede bayesiana com a probabilidades para cada caso.
		FiniteNode reside = new FullCPTNode(Task7RV.Reside , new double[]{
				
				//DEZ
				0.70, 0.30,
				
				//QUINZE
				0.65, 0.35,
				
				//VINTE
				0.60, 0.40,
				
				//VINTE E CINCO
				0.55, 0.45,
				
				//TRINTA
				0.50, 0.50, 
				
				//TRINTA E CINCO
				0.45, 0.55, 
				
				//QUARENTA	
				0.40, 0.60, 
				
				//QUARENTA E CINCO
				0.20, 0.80, 
				
				//CINQUENTA
				0.10, 0.90
			}, bolsaNode);
		
		// Cria nó da rede bayesiana com a probabilidades para cada caso.
		FiniteNode imovel = new FullCPTNode(Task7RV.Imovel , new double[]{

				//DEZ
				0.70, 0.30,
				
				//QUINZE
				0.65, 0.35,
				
				//VINTE
				0.60, 0.40,
				
				//VINTE E CINCO
				0.55, 0.45,
				
				//TRINTA
				0.50, 0.50, 
				
				//TRINTA E CINCO
				0.45, 0.55, 
				
				//QUARENTA	
				0.40, 0.60, 
				
				//QUARENTA E CINCO
				0.20, 0.80, 
				
				//CINQUENTA
				0.10, 0.90
			}, bolsaNode);
		
		// Cria nó da rede bayesiana com a probabilidades para cada caso.
		FiniteNode doencaGrave = new FullCPTNode(Task7RV.DoencaGrave , new double[]{

				//DEZ
				0.40, 0.60,
				
				//QUINZE
				0.45, 0.55,
				
				//VINTE
				0.50, 0.50,
				
				//VINTE E CINCO
				0.55, 0.45,
				
				//TRINTA
				0.60, 0.40, 
				
				//TRINTA E CINCO
				0.65, 0.35, 
				
				//QUARENTA	
				0.70, 0.30, 
				
				//QUARENTA E CINCO
				0.75, 0.25, 
				
				//CINQUENTA
				0.80, 0.20
			}, bolsaNode);
		
		// Cria nó da rede bayesiana com a probabilidades para cada caso.
		FiniteNode fase = new FullCPTNode(Task7RV.Fase , new double[]{

				//DEZ
				0.54, 0.36, 0.10,
				
				//QUINZE
				0.56, 0.35, 0.09,
				
				//VINTE
				0.58, 0.34, 0.08,
				
				//VINTE E CINCO
				0.60, 0.33, 0.07,
				
				//TRINTA
				0.62, 0.32, 0.06,
				
				//TRINTA E CINCO
				0.64, 0.31, 0.05,
				
				//QUARENTA	
				0.66, 0.30, 0.04,
				
				//QUARENTA E CINCO
				0.68, 0.29, 0.03,
				
				//CINQUENTA
				0.70, 0.28, 0.02
			}, bolsaNode);
		
		// Cria nó da rede bayesiana com a probabilidades para cada caso.
		FiniteNode segundoGrauPublica = new FullCPTNode(Task7RV.SegundoGrau , new double[]{

				//DEZ
				0.10, 0.90,
				
				//QUINZE
				0.13, 0.87,
				
				//VINTE
				0.25, 0.75,
				
				//VINTE E CINCO
				0.55, 0.45,
				
				//TRINTA
				0.60, 0.40, 
				
				//TRINTA E CINCO
				0.65, 0.35, 
				
				//QUARENTA	
				0.70, 0.30, 
				
				//QUARENTA E CINCO
				0.80, 0.20, 
				
				//CINQUENTA
				0.90, 0.10
			}, bolsaNode);
		
		
		//REDE BAYESEANA
		BayesNet task7BN = new BayesNet(bolsaNode, rendaFamiliarNode, trabalhaNode, conjugeNode, irmao, reside, imovel, doencaGrave, fase, segundoGrauPublica);
		
		// Define variáveis observadas.
		AssignmentProposition rendaFamiliarAP = new AssignmentProposition(Task7RV.RendaFamiliar, "UmaTresSM");
		AssignmentProposition trabalhoAP = new AssignmentProposition(Task7RV.Trabalha, true);
		AssignmentProposition conjugeAP = new AssignmentProposition(Task7RV.Conjuge, true);
		AssignmentProposition irmaoAP = new AssignmentProposition(Task7RV.Irmao, true);
		AssignmentProposition resideLagesAP = new AssignmentProposition(Task7RV.Reside, false);
		AssignmentProposition imovelAP = new AssignmentProposition(Task7RV.Imovel, false);
		AssignmentProposition doencaAP = new AssignmentProposition(Task7RV.DoencaGrave, true);
		AssignmentProposition faseAP = new AssignmentProposition(Task7RV.Fase, "PrimeiraAQuarta");
		AssignmentProposition segundoGrauPublicaAP = new AssignmentProposition(Task7RV.SegundoGrau, true);
		
		AssignmentProposition[] rsap = new AssignmentProposition[]{rendaFamiliarAP, trabalhoAP, conjugeAP, irmaoAP, resideLagesAP, imovelAP, doencaAP, faseAP, segundoGrauPublicaAP};
		
		// Define variável consultada.
		RandVar[] rsrv = new RandVar[]{Task7RV.Bolsa};

		// EnumerationAsk
		EnumerationAsk ea = new EnumerationAsk();
		CategoricalDistribution returnEnumerationAsk = ea.enumerationAsk(rsrv, rsap, task7BN);
		System.out.println("EnumerationAsk: " + returnEnumerationAsk);
		
		// EliminationAsk
		EliminationAsk ela = new EliminationAsk();
		CategoricalDistribution returnEliminationAsk = ela.eliminationAsk(rsrv, rsap, task7BN);
		System.out.println("EliminationAsk: " + returnEliminationAsk);
		
		// Prior-Sample
		PriorSample ps = new PriorSample();
		Map<RandomVariable, Object> resultPS = ps.priorSample(task7BN);
		System.out.println("Prior Sample: " + resultPS);
		
		// Rejection-Sampling
		RejectionSampling rs = new RejectionSampling();
		CategoricalDistribution cd = rs.rejectionSampling(rsrv, rsap, task7BN, 1000000);
		System.out.println("Rejection Sampling: " + cd);
	}
}
