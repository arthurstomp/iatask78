package ufs.br.ai.task7;

import aima.core.probability.domain.ArbitraryTokenDomain;
import aima.core.probability.domain.BooleanDomain;
import aima.core.probability.util.RandVar;

public class Task7RV {
	public static final RandVar Bolsa = new RandVar("Bolsa", 
			new ArbitraryTokenDomain("Dez","Quinze","Vinte","VinteECinco","Trinta","TrintaECinco","Quarenta","QuarentaECinco","Cinquenta"));
	
	public static final RandVar RendaFamiliar = new RandVar("RendaFamiliar", 
			new ArbitraryTokenDomain("UmaTresSM","QuatroADezSM","DezSMAcima"));
	
	public static final RandVar Trabalha = new RandVar("Trabalha", 
			new BooleanDomain() );
	
	public static final RandVar Conjuge = new RandVar("ConjugeEstudaIES", 
			new BooleanDomain() );
	
	public static final RandVar Irmao = new RandVar("IrmaoEstuda", 
			new BooleanDomain() );
	
	public static final RandVar Reside = new RandVar("ResideLajes", 
			new BooleanDomain() );
	
	public static final RandVar Imovel = new RandVar("TemImovel", 
			new BooleanDomain() );
	
	public static final RandVar DoencaGrave = new RandVar("TemDoencaGrave", 
			new BooleanDomain() );
	
	public static final RandVar Fase = new RandVar("FaseEstuda", 
			new ArbitraryTokenDomain("PrimeiraAQuarta","QuintaASetima","SetimaAcima"));
	
	public static final RandVar SegundoGrau = new RandVar("CursouSegundoGrauPublica", 
			new BooleanDomain() );
}
